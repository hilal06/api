import slack
import os
from pathlib import Path
from dotenv import load_dotenv

env_path= Path('.')/'.env'
load_dotenv(dotenv_path=env_path)

def SendMessage(message):
    client=slack.WebClient(token=os.environ['SLACK_TOKEN'])
    client.chat_postMessage(channel='#test',text=message)

#send slack message using slack api
