import os
from googleapiclient.http import MediaFileUpload
from Google import Create_Service
from SlackMessage import SendMessage
drive_service=Create_Service('drive','v3')

def createFolder(name):
    file_metadata = {
    'name': name,
    'mimeType': 'application/vnd.google-apps.folder'
    }
    file = drive_service.files().create(body=file_metadata,
                                        fields='id').execute()
    print ('Folder ID: %s' % file.get('id'))
    return file.get('id')

def convertExcel(file_path: str,folder_id: list=None):
    if not os.path.exists(file_path):
        print(f'{file_path} not found')
        return
    try:
        file_metadata={
            'name': os.path.splitext(os.path.basename(file_path))[0],
            'mimeType': 'application/vnd.google-apps.spreadsheet',
            'parents': folder_id
            }
        media = MediaFileUpload(
            filename=file_path,
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response=drive_service.files().create(
            media_body=media,
            body=file_metadata
            ).execute()
        print(response)
        return response
    except Exception as e:
        print(e)
        return

sheet_service=Create_Service('sheets','v4')
def creatSheets(sheet_id):
    sheet=sheet_service.spreadsheets()
    names=sheet.values().get(spreadsheetId=sheet_id,range='A1:F1').execute().get('values',[])
    result=sheet.values().get(spreadsheetId=sheet_id,range='A2:F14').execute()
    values=result.get('values',[])
    for row in values:
        spreadsheet={
            'properties':{
                'title' :row[0]
                }
            }
        spreadsheet=sheet.create(body=spreadsheet,fields='spreadsheetId').execute()
        print(names)
        print(row)
        body={'values': [names[0],row]}
        sheet.values().update(
            spreadsheetId=spreadsheet.get('spreadsheetId'),range="Sheet1!A1", valueInputOption="USER_ENTERED", body=body
            ).execute()
        body={'values': [['Total Budget'],[float(row[2])*float(row[4])]]}
        sheet.values().update(
            spreadsheetId=spreadsheet.get('spreadsheetId'),range="Sheet1!G1", valueInputOption="USER_ENTERED", body=body
            ).execute()


folder_id=createFolder('caseStudy')
SendMessage('Folder is successfully created')
response=convertExcel('data.xlsx',[folder_id])
SendMessage('Dataset is successfully uploaded')
creatSheets(response.get('id'))
SendMessage('For each campaign Total Budget is calculated and spreadsheet is successfully created')
